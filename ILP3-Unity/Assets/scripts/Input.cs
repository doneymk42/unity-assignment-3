﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input : MonoBehaviour
{
    public GameObject Userselecteddice;

    public float timeStartedLerping;
    public float lerpTime;
    private bool shouldLerp = false;
    public Vector2 endPos;
    public Vector2 startPos;
    int PlaySpaceCheck = 0;

    void Start()
    {
        // initiate starting position
        startPos = transform.position;
    }

    void Update()
    {
        // if the the player clicks on a die, then lerp = true, meaning it will change its position to the bottom
        if (shouldLerp == true)
        {
            transform.position = Lerp(startPos, endPos, timeStartedLerping, lerpTime);
        }
    }

    // vector lerp to dictate how chosen prefab will move/change positions
    public Vector3 Lerp(Vector3 start, Vector3 end, float timeStartedLerping, float lerpTime = 1)
    {
        float timeSinceStarted = Time.time - timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;
        var result = Vector3.Lerp(start, end, percentageComplete);
        return result;
    }

    void OnMouseDown()
    {
        timeStartedLerping = Time.time;
        // score changes when player input collides with prefab
        Score.scoreValue += 1;
        // position change become true when player input is applied
        shouldLerp = true;
        PlaySpaceCheck++;
        if (PlaySpaceCheck == 2)
        {
            startPos = endPos;
            endPos = new Vector2(0, -10);
        }
    }