﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Dice
{
    public Sprite sprite;
    public int val;
    public Color colour;
    public int row, col;

    // Create default constructor
    public Dice(int row,int col)
    {

    }
}

public class spawn : MonoBehaviour
{
    public GameObject dicePrefab;
    public Sprite[] dicespr; 
    public Dice[][] setup;

    private void Start()
    {
        int row = 2, col = 5;
        setup = new Dice[row][col];

        // 0) Initialize the array
        // 1) Create 2 rows of 5 dice, each with random value and colour
        for(int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                setup[i][j] = new Sprite();
                setup[i][j] = randomdice();
                setup[i][j].row = row;
                setup[i][j].col = col;
            }
        }
                
        // 2) Make sure all 10 dice are properly aligned on screen in 2 rows of evenly spaced 5 dice
    }

    // This function always creates and returns a Dice object with:
    // 1) a random value from 1 to 6, obviously
    // 2) a random colour, selected from 6 available colours
    public Dice randomdice()
    {
        Dice rand_die = new Dice();
        int arrayidx = Random.Range(0, dicespr.Length);
        int rand_colour = Random.Range(0, 6); // Get a colour from 0 to 5
        Sprite diceSprite = new Sprite();
        diceSprite = dicespr[arrayidx];
        Color colour = array_colours[rand_colour]; // Index into array containing all 6 Colour objects/ properties

        GameObject obj = Instantiate(dicePrefab);

        obj.GetComponent<SpriteRenderer>().sprite = diceSprite;
        diceSprite.setColour(colour);   // Set the sprites colour

        rand_die.sprite = diceSprite; // This sprite was created randomly from an array of dice sprite, and an array of colours
        rand_die.val = arrayidx + 1; // This value comes the random value above
        rand_die.colour = colour; // THis comes from the random value above

        return rand_die;
    }
    
}